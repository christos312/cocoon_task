<!doctype html>
<html>
<head>
    @include('inc.head')
</head>
<body>
<header>
    @include('inc.adminheader')
</header>
<div class="container">

    @yield('content')



    <footer class="row">
        @include('inc.footer')
    </footer>

</div>
</body>
</html>