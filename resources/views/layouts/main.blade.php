<!doctype html>
<html>
<head>
    @include('inc.head')
</head>
<body>
<div class="container">

    <header class="row">
        @include('inc.header')
    </header>



    @yield('content')



    <footer class="row">
        @include('inc.footer')
    </footer>

</div>
</body>
</html>