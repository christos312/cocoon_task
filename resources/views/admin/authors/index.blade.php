@extends('layouts.admin')
@section('content')
    <h1>Authors</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="action-bar">
                <a  href="{{ route('authors.create') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
    @if(session('status') )
        <div class="dismissible-alert alert alert-success">
            <strong>{{session('status')}}</strong>
        </div>
    @endif
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">Author Name</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($authors as $author)
            <tr>
                <td>{{$author->full_name}}</td>
                <td>
                    <a href="{{ route('authors.edit', $author->id) }}" class="btn btn-primary"><span class="oi oi-pencil"></span></a>
                    {{ Form::open(array('route' => array('authors.destroy', $author->id), 'method' => 'DELETE', 'class' => 'delete-form')) }}
                    <button class="btn btn-danger">
                        <span class="oi oi-delete"></span>
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach

    </table>
@stop

