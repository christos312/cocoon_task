@extends('layouts.admin')
@section('content')
    <h1>Create new Author</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <strong>We found the following error, please fix before saving: </strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::open(array('route' => 'authors.store')) }}


    <div class="form-group @if($errors->has('firstname')) error @endif">
        {{ Form::label('firstname', 'First Name') }}
        {{ Form::text('firstname', '', array('class' => 'form-control')) }}
    </div>
    <div class="form-group @if($errors->has('lastname')) error @endif">
        {{ Form::label('lastname', 'Last Name') }}
        {{ Form::text('lastname', ' ', array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
    <a href="{{route('authors.index')}}" class="btn btn-secondary">Cancel</a>

    {{ Form::close() }}
@stop




