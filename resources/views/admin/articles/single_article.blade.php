@extends('layouts.main')
@section('content')
    <div class="jumbotron">
        <h1 class="display-4">{{$article->title}}</h1>
        <p class="lead">{{$article->summary}}</p>
        <hr class="my-4">
        <p>{{$article->content}}</p>
        <p class="lead">
            <div class="row">
            <div class="col-md-3">
                <a class="btn btn-primary" href="{{route('admin')}}" role="button">
                    <span class="oi oi-chevron-left"></span>
                    Back
                </a>
            </div>
                <div class="col-md-3">
                    <button class="btn btn-primary" href="{{ route('articles.edit', $article->id) }}" role="button">
                        <span class="oi-icon oi oi-pencil"></span>
                        Update
                    </button>
                </div>
                <div class="col-md-3">
                    {{ Form::open(array('route' => array('articles.destroy', $article->id), 'method' => 'DELETE')) }}
                    <button class="btn btn-danger">
                        <span class="oi-icon oi oi-delete"></span>
                        Delete
                    </button>
                    {{ Form::close() }}

                </div>
            </div>

        </p>
    </div>
@stop




