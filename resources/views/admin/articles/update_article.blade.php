@extends('layouts.admin')
@section('content')
    <h1>Update article</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <strong>We found the following error, please fix before saving: </strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::model($article, array('route' => array('articles.update', $article->id), 'method' => 'PUT')) }}


    <div class="form-group @if($errors->has('title')) error @endif">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group @if($errors->has('summary')) error @endif">
        {{ Form::label('summary', 'Summary') }}
        {{ Form::text('summary', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group @if($errors->has('content')) error @endif">
        {{ Form::label('content', 'Content') }}
        {{ Form::textarea('content', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('publishstatus', 'Status') }}
        {{ Form::select('publishstatus', array(0=>"Unpublished", 1=>"Published", 2=>"Draft"), null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('author_id', 'Author') }}
        {{ Form::select('author_id', $authors, null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('category_id', 'Category') }}
        {{ Form::select('category_id', $categories, null, array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
    <a href="{{route('admin')}}" class="btn btn-secondary">Cancel</a>

    {{ Form::close() }}
@stop




