@extends('layouts.admin')
@section('content')

    <div class="alert alert-danger">
        <strong>Ohh snap, something went wrong</strong>
        <p>{{$message}}</p>
        <p><strong><a href="{{route('admin')}}">Return back</a></strong></p>
    </div>
@stop




