@extends('layouts.admin')
@section('content')
    <h1>Update Category</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <strong>We found the following error, please fix before saving: </strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::model($category, array('route' => array('categories.update', $category->id), 'method' => 'PUT')) }}


    <div class="form-group @if($errors->has('categoryname')) error @endif">
        {{ Form::label('categoryname', 'Category Name') }}
        {{ Form::text('categoryname', null, array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
    <a href="{{route('categories.index')}}" class="btn btn-secondary">Cancel</a>

    {{ Form::close() }}
@stop




