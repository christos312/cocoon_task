@extends('layouts.admin')
@section('content')

    <h1>Categories</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="action-bar">
                <a  href="{{ route('categories.create') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
    @if(session('status') )
        <div class="dismissible-alert alert alert-success">
            <strong>{{session('status')}}</strong>
        </div>
    @endif
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">Category Name</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->categoryname}}</td>
                <td>
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary"><span class="oi oi-pencil"></span></a>
                    {{ Form::open(array('route' => array('categories.destroy', $category->id), 'method' => 'DELETE', 'class' => 'delete-form')) }}
                    <button class="btn btn-danger">
                        <span class="oi oi-delete"></span>
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach

    </table>
@stop

