@extends('layouts.admin')
@section('content')
    <h1>Create new Category</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <strong>We found the following error, please fix before saving: </strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{ Form::open(array('route' => 'categories.store')) }}


    <div class="form-group @if($errors->has('categoryname')) error @endif">
        {{ Form::label('categoryname', 'Category Name') }}
        {{ Form::text('categoryname', '', array('class' => 'form-control')) }}
    </div>
    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
    <a href="{{route('categories.index')}}" class="btn btn-secondary">Cancel</a>

    {{ Form::close() }}
@stop




