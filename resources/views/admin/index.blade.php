@extends('layouts.admin')

@section('content')

    <h1>Articles</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="action-bar">
                <a  href='{{ route('articles.create') }}' class="btn btn-primary">Compose New</a>
            </div>
        </div>
    </div>
    @if(session('status') )
        <div class="dismissible-alert alert alert-success">
            <strong>{{session('status')}}</strong>
        </div>
    @endif
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Summary</th>
            <th scope="col">Status</th>
            <th scope="col">Author</th>
            <th scope="col">Category</th>
            <th scope="col">Published</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>{{$article->title}}</td>
                <td>{{$article->summary}}</td>
                <td>
                    @if($article->publishstatus == 0)
                        Unpublished
                    @elseif($article->publishstatus == 1)
                        Published
                    @else
                        Draft
                    @endif
                </td>
                <td>{{$article->author->full_name}}</td>
                <td>{{$article->category->categoryname}}</td>
                <td>{{$article->created_at}}</td>
                <td>
                    <a href='{{ route('articles.show', $article->id) }}' class="btn btn-primary"><span class="oi oi-eye"></span></a>
                    <a href="{{ route('articles.edit', $article->id) }}" class="btn btn-primary"><span class="oi-icon oi oi-pencil"></span></a>
                    {{ Form::open(array('route' => array('articles.destroy', $article->id), 'method' => 'DELETE', 'class' => 'delete-form')) }}
                    <button class="btn btn-danger">
                        <span class="oi oi-delete"></span>
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach

    </table>
@stop

