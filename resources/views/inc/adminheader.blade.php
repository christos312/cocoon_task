
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{(Request::is('admin')?'active':'')}}">
                    <a class="nav-link" href="{{route('admin')}}">Articles <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{(Request::segment(2) == "authors"?'active':'')}}">
                    <a class="nav-link" href="{{route('authors.index')}}">Authors</a>
                </li>
                <li class="nav-item {{(Request::segment(2) == "categories"?'active':'')}}">
                    <a class="nav-link" href="{{route('categories.index')}}">Categories</a>
                </li>
            </ul>
        </div>
    </div>

</nav>