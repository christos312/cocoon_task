@extends('layouts.main')
@section('content')
    <div class="row font-page">
        <div class="col-md-12 text-center">
            <h1>Welcome to Cocoon test app</h1>
            <h2><a href="{{route('admin')}}">Go to Admin</a></h2>
            <h2><a href="{{url('/api')}}">Go to API</a></h2>

            <p><strong>Available API end points</strong></p>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Method</th>
                    <th scope="col">URI</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        GET|HEAD
                    </td>
                    <td>
                        api/articles
                    </td>
                </tr>
                <tr>
                    <td>
                        GET|HEAD
                    </td>
                    <td>
                        api/articles/{id}
                    </td>
                </tr>
                </tbody>
            </table>


        </div>
    </div>

@stop
