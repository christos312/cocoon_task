<?php

namespace App\Http\Controllers\admin;

use App\Article;
use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminIndexController extends Controller
{
    public function index(){
    	//get all articles from the database and sort them by creation date

    	$articles = Article::all()->sortBy('created_at');

    	//then return the view in admin/index and pass the articles to the view
    	return view('admin/index')->with('articles', $articles);
    }
}
