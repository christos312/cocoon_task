<?php

namespace App\Http\Controllers\admin;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $authors = Author::all();

	    return view('admin/authors/index')->with('authors', $authors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin/authors/create_author');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $validations = $request->validate([
		    'firstname' => 'required|max:100',
		    'lastname' => 'required|max:100',
	    ]);

	    $requestData = $request->all();

	    $newAuthor = new Author();
	    $newAuthor->fill($requestData);

	    if(!$newAuthor->save()){
		    return view('error', "Error while trying to save author.");
	    }

	    return redirect(route('authors.index'))->with('status', 'Author added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    return redirect(route('authors.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $author = Author::find($id);
	    return view('admin/authors/update_author')->with(array('author'=> $author));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $validations = $request->validate([
		    'firstname' => 'required|max:100',
		    'lastname' => 'required|max:100',
	    ]);

	    $author = Author::find($id);

	    if(!$author->update($request->all()))
		    return view('error', "Error while trying to save author.");

	    return redirect(route('authors.index'))->with('status', 'Author updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    $author = Author::find($id);

	    //We need to make sure that when deleting an auhtor there are no
	    //articles related to it, so we count how many articles are related
	    //to the author we are trying to delete.
	    $numOfArticles = $author->articles()->get()->count();
	    //in case the articles are more than 0, then we return an error
	    //letting the user know that the specific author cannot be deleted since there
	    //are articles related to it.
	    if($numOfArticles > 0)
		    return view('admin/error', array('message'=> "Cannot delete author, there is/are " . $numOfArticles . " assigned articles to this author."));

	    //if no articles are related then we try to delete, in case of an error we return an error
	    if(!$author->delete())
		    return view('error', "Error while trying to delete author.");

	    //if delete is successfull we return back to authors
	    return redirect(route('authors.index'))->with('status', 'Author deleted!');
    }
}
