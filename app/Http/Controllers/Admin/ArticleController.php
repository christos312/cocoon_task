<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Author;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	//no need to do anything here, since we present all articles in the admin
	    //page, so we just redirect to admin
	    return redirect(route('admin'));
    }

    /**
     * This function will present the form for the creation if a new article
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	//We need to get all authors and categories so that we present them as a selectbox
	    //in the creation form

	    //we get all authors and we create an array with the id and value
	    //eg 1=>Author One, 2=> Author Two
		$authors = Author::all()->pluck('full_name', 'id');
		$categories = Category::all()->pluck('categoryname', 'id');;

		//then we return the article create article view with the authors and categories
	    //this will load the view in /resources/views/admin/articles
        return view('admin/articles/create_article')->with(array('authors'=>$authors, 'categories' => $categories));
    }

    /**
     * This function is responsible to save the article into the DB
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	//We set some validations before we store the object to the DB
	    //validate will return the proper response to the view in case
	    //one of these rules fails
	    $request->validate([
		    'title' => 'required|max:100',
		    'summary' => 'required|max:200',
			'content' => 'required'
	    ]);

	    //if validation is passed then we get al form data.
	    $requestData = $request->all();

	    //Create an article object and fill it with the form data.
	    $newArticle = new Article();

	    //Fill we only fill data that we didnt guard in the Model definition
	    //The model is defined in folder /app/Article.php
	    $newArticle->fill($requestData);

	    //Try to save the article in the DB, in case there is an error return
	    //the error view and give appropriate message
	    if(!$newArticle->save())
	    	return view('error', "Error while trying to save article.");

	    //If article was successfully saved redirect back to admin index and
	    //show a message to the user
	    return redirect(route('admin'))->with('status', 'Article added!');;

    }

    /**
     * Display the specified article.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	//Find the article in the DB give then id.
        $article = Article::find($id);

        //Return the single_article view located in /resources/views/admin/articles and
	    //send the article as data to the view
        return view('admin/articles/single_article')->with('article', $article);
    }

    /**
     * Display the editing form for an article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    	//find the article we want to edit from DB so that we can fill
	    // the input fields with the current article data
	    $article = Article::find($id);
	    //Get all authors from the DB
	    $authors = Author::all()->pluck('full_name', 'id');
	    //Get all categories
	    $categories = Category::all()->pluck('categoryname', 'id');;

	    //return the view with the editing form and the data needed to fill in the form
	    return view('admin/articles/update_article')->with(array('article'=> $article, 'authors'=>$authors, 'categories' => $categories));

    }

    /**
     * Update the specific article in the DB.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    //validate the form
    	$request->validate([
		    'title' => 'required|max:100',
		    'summary' => 'required|max:200',
		    'content' => 'required'
	    ]);

    	//find the article from the DB
	    $article = Article::find($id);

	    //if update fails return an error
	    if(!$article->update($request->all()))
	        return view('error', "Error while trying to update article.");

	    //if update is successfull return to the admin index
	    return redirect(route('admin'))->with('status', 'Article updated!');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	//find the article we wish to delete
	    $article = Article::find($id);

	    //try to delete if from DB, if failed return and error
	    if(!$article->delete())
		    return view('error', "Error while trying to delete article.");

	    //if delete is successfull return to admin
	    return redirect(route('admin'))->with('status', 'Article deleted!');
    }
}
