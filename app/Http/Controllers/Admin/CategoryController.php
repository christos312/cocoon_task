<?php

namespace App\Http\Controllers\admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $categories = Category::all();

	    return view('admin/categories/index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    return view('admin/categories/create_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $validations = $request->validate([
		    'categoryname' => 'required|max:100'
	    ]);

	    $requestData = $request->all();

	    $newCat = new Category();
	    $newCat->fill($requestData);

	    if(!$newCat->save()){
		    return view('error', "Error while trying to save category.");
	    }

	    return redirect(route('categories.index'))->with('status', 'Category added!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    return redirect(route('categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $cat = Category::find($id);
	    return view('admin/categories/update_categories')->with(array('category'=> $cat));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $validations = $request->validate([
		    'categoryname' => 'required|max:100'
	    ]);

	    $cat = Category::find($id)->update($request->all());


	    return redirect(route('categories.index'))->with('status', 'Category updated!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    //We need to make sure that when deleting a category there are no
	    //articles related to it, so we count how many articles are related
	    //to the category we are trying to delete.
	    $cat = Category::find($id);
		$numOfArticles = $cat->articles()->get()->count();
	    //in case the articles are more than 0, then we return an error
	    //letting the user know that the specific category cannot be deleted since there
	    //are articles related to it.
	    if($numOfArticles > 0)
		    return view('admin/error', array('message'=> "Cannot delete category, there are " . $numOfArticles . " assigned articles to this category."));

	    if(!$cat->delete())
		    return view('error', "Error while trying to delete category.");

	    return redirect(route('categories.index'))->with('status', 'Category deleted!');
    }
}
