<?php

namespace App\Http\Controllers\API;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
	//Return all articles in json format
	public function index()
	{
		//Select all articles and also load the author and category data.
		//Sample return
		$articles = Article::with(array('author', 'category'))->get();

		return response()->json($articles);
	}

	//Return a specific article given the id
	public function show($id)
	{
		$article = Article::with(array('author', 'category'))->find($id);

		return response()->json($article);
	}
}
