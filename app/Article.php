<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'articles';
	protected $guarded = ['id', 'created_at', 'updated_at']; //safegurd that these attributes will not be fillable by anyone

	public function author()
	{
		return $this->belongsTo('App\Author');
	}

	public function category()
	{
		return $this->belongsTo('App\Category');
	}
}
