<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories';
	protected $guarded = ['id', 'created_at', 'updated_at']; //safegurd that these attributes will not be fillable by anyone

	public function articles()
	{
		return $this->hasMany('App\Article');
	}
}
