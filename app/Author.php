<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
	protected $table = 'authors';
	protected $guarded = ['id', 'created_at', 'updated_at']; //safegurd that these attributes will not be fillable by anyone


	function articles()
	{
		return $this->hasMany('App\Article');
	}

	public function getFullNameAttribute($value)
	{
		return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
	}
}
