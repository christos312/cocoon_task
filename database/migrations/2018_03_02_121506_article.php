<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Article extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('articles', function (Blueprint $table) {
		    $table->increments('id');
		    $table->char('title', 100);
		    $table->char('summary', 200);
		    $table->text('content');
		    $table->tinyInteger('publishstatus');
		    $table->integer('author_id')->unsigned();
		    $table->integer('category_id')->unsigned();

		    $table->foreign('author_id')->references('id')->on('authors');
		    $table->foreign('category_id')->references('id')->on('categories');

		    $table->timestamps();

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('articles');
    }
}
