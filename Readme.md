#Cocoon Test App

This app is a demo app that exposes an API endpoint to be consumed by the client. It also provides a back-end web app for creating/updating the content
##Table of contents
    
* Prerequisite
    * Windows
    * MAC OS
    * Git
* Installation
* Souce code explanation
    * Routes
    * Migrations
    * Models
    * Controllers 
    * Views

## Prerequisite

### Windows
#### PHP and Mysql
In case you dont have PHP and MySQL installed on your machine, please download and install them.

For Windows users its easier to download WAMP, which inlcudes all packages necessesary to run the app.

Download [here](http://www.wampserver.com/en/#download)

#### Composer

In order to install Laravel framework we will need to download and install composer.

Download and install composer from [here](https://getcomposer.org/)

#### Git

Install Git on Windows, from [here](https://git-scm.com/download/win)

### MAC OS
#### PHP and Mysql

In case you dont have PHP and MYSQL installed on your machine, please download and install them.

For MAC users its easier to download AMPPS, which includes all packages necessary to run the app.

Download [here](http://www.ampps.com/download)

#### Composer

To install composer to the current directory run the following
```bash
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  php composer-setup.php
  php -r "unlink('composer-setup.php');"
```

#### Git

Install Git on MAC, from [here](https://git-scm.com/download/mac)


## Installation

First thing first we need to create our database.

Open a browser and navigate to

```
http://localhost/phpmyadmin
```

Login using the default credentials

(WINDOWS: username: root, password: leave it empty)
(MAC:     username:root, password:mysql) This may differ based on how you installed MySQL

Click New from the left sidebar to create a new DB and name it **cocoon**, then click **Create**

![DB][createdbimg]

Now we need to get the source files


Open cmd or terminal and clone the repository to your machine

```
git clone https://christos312@bitbucket.org/christos312/cocoon_task.git
```

Then change directory to the folder cocoon_task

```
cd cocoon_task
```

Run the following command to install all dependacies, be patients this might take some time, prepare a coffee until is done :)
```
composer install
```
If you are trying to install on MAC OS, run the command as below. Please make sure that you have composer installed on the directory of the project

```
php composer.phar install
```

Create .env file, here we will specify our enviroment. Add the following info and make sure that the info here matches your enviroment. **Make sure that your
.env file does not have any spaces or comments. Its a good idea to rename .env.example to .env and use it that way.**
```
 APP_NAME=Laravel
 APP_ENV=local
 APP_KEY=                   #this will be filled in the enxt step
 APP_DEBUG=true
 APP_URL=http://localhost   #change this based on your webserver
 
 LOG_CHANNEL=stack
 
 DB_CONNECTION=mysql
 DB_HOST=127.0.0.1          #change this in case MYSQL is running on another IP
 DB_PORT=3306               #change this in case MYSQL is running on another port
 DB_DATABASE=cocoon         #This is the database name, change it if necessaray
 DB_USERNAME=root           #This is the username to connect to DB, change it if necessaray
 DB_PASSWORD=               #This is the password to connect to DB, change it if necessaray
 DB_SOCKET=/Applications/AMPPS/var/mysql.sock #add this only if you are on a MAC and using AMPPS. if you are using something else you need to find the path for mysql.sock
```

Then we need to create a key for the Laravel app. Run this command
```
 php artisan key:generate
```

Then we need to create the tables needed for the app to run. To do this run the command below
```
 php artisan migrate
```
Finally we are ready to run the app. Run the command to start serving the app
```
 php artisan serve
```

If everything is ok you should get a result like the one below

```
Laravel development server started: <http://127.0.0.1:8000>
```

Open browser and go to the url to see the app


## Source code explanation

For this project I choose to use a PHP framework called Laravel. Laravel is an MVC framework, a model, view controller framework.
This framework seperates conserns of the representation of the app, the data and the processing of the data. Since a Laravel project
contains a lot of files, below I will list important files for this app

#### Routes

This app defines 2 kinds of routes. One for the Web app and one for the API. The routes are defined in the files below

**Files**

* \routes\api.php
* \routes\web.php

![routes][routestable]

#### Migrations

Migrations are used to create the database tables for the app. In migrations we can define the structure of a table in the DB. An example of a create table is below

```
Schema::create('authors', function (Blueprint $table) { //create a table called authors
    $table->increments('id');                           //its primary key will be id and its going to be auto incremented
    $table->char('firstname', 100);                     //it should have a column firstname which is a string of 100 characters
    $table->char('lastname', 100);

    $table->timestamps();
});
```

In case a table needs to have relations with other tables in the DB we can define this like below

```
$table->integer('author_id')->unsigned();                       //we create a column  for the foreing key
$table->foreign('author_id')->references('id')->on('authors');  //we define that the foreing key references the column id on the authors table
```
**Files**

* database/migrations/2018_03_02_114345_author.php
* database/migrations/2018_03_02_121329_category.php
* database/migrations/2018_03_02_121506_article.php


#### Models

Models are used to define the data models we need for this app, based on the description there are 3 models for this project.
Article, Category and Author. If we wanted to have more flexibility we could create a fourth one for the Publish Status

Below is some examples of a models

```
class Article extends Model
{
	protected $table = 'articles';                            //we define that the table for this model is the table articles
	protected $guarded = ['id', 'created_at', 'updated_at'];  //we define some attributes that we want to guard, meaning that
	                                                          //the client will not be allowed to set them, we are going to handle that
	                                                          //since these attributes are autogenerated

	public function author()                                  //an article can have one author, so we define a finction author
	{                                                         //and we say that an Article belongsTo one Auhtor, Author being the model
		return $this->belongsTo('App\Author');
	}

	public function category()                                //An Article also has a category
	{
		return $this->belongsTo('App\Category');
	}
}

class Author extends Model
{
	protected $table = 'authors';
	protected $guarded = ['id', 'created_at', 'updated_at'];


	function articles()                                     //Here we define that one author has many Articles
	{                                                       //we do that by defining a function called articles
		return $this->hasMany('App\Article');               //and say that an Author hasMany Articles
	}

	public function getFullNameAttribute($value)            //this is a custom attribute function that returns the fullname of the Author
	{
		return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
	}
}
```
 **Files**
 
 * app/Article.php
 * app/Author.php
 * app/Category.php

#### Controllers 

Controllers are used to process data and communicate with the DB then they pass the result to the view for presentation.
Contorllers should not care about how the date will be presented thats why presentation should not be included in these files.
Documentation for the controllers can be found in the controller file itself. 

In few words, I have created 3 resouce controlles.

Resource controllers create routes for CRUD operations, the following routes are defined



**Files**

* app\Http\Controllers\Admin\AdminIndexController.php
* app\Http\Controllers\Admin\ArticleController.php
* app\Http\Controllers\Admin\AuthorController.php
* app\Http\Controllers\Admin\CategoryController.php
* app\Http\Controllers\API\ArticleController.php



#### Views

Views is all about presentation to the user. The view should not care where the data are coming from or how to process data. They role is to receive the data from somewhere and present them. Views are also responsible to pass
data of the user to the Controllers for processing. Views dont process data.

First we define the layout that will be used for all pages

```
<!doctype html>
<html>
<head>
    @include('inc.head')
</head>
<body>
<header>
    @include('inc.adminheader')
</header>
<div class="container">

    @yield('content')



    <footer class="row">
        @include('inc.footer')
    </footer>

</div>
</body>
</html>
```

In the inc folder we define all common elements like the header, the footer, the navitation and the head, where we
will add all assets. In the head we can define assets like stylesheets or scripts

**Files**

* resources\views\admin\articles\create_article.blade.php
* resources\views\admin\articles\single_article.blade.php
* resources\views\admin\articles\update_article.blade.php
* resources\views\admin\authors\create_author.blade.php
* resources\views\admin\authors\index.blade.php
* resources\views\admin\authors\update_author.blade.php
* resources\views\admin\categories\create_category.blade.php
* resources\views\admin\categories\index.blade.php
* resources\views\admin\categories\update_categories.blade.php
* resources\views\admin\error.blade.php
* resources\views\admin\index.blade.php
* resources\views\inc\adminheader.blade.php
* resources\views\inc\footer.blade.php
* resources\views\inc\head.blade.php
* resources\views\inc\header.blade.php
* resources\views\layouts\admin.blade.php
* resources\views\layouts\main.blade.php

[createdbimg]: gitreadmeassets/create_db.JPG
[routestable]: gitreadmeassets/routestable.JPG