<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * in this file we will define the routes for the admin panel
 * This routes will be accessed from a browser to handle the
 * backend content of the system.
 *
 * Authentication has been ignored here since this is a demo
 * App and its not meant to be used in real life situations.

*/
Route::get('/', 'IndexController@index');

/*
 * Here we define a group of routes with prefix admin, so that
 * user will be able to access all sub-routes with the URL
 * ourdomain/admin/*
 *
 * We also define the namespace as Admin, so that we can have all
 * our Controllers under the Admin folder for better organization
 * /Http/Controllers/Admin
 *
 * Here we will define resource routes, which help us to assign
 * typical CRUD (Create, Read, Update, Delete) routes to a controller
 *
 * More info here: https://laravel.com/docs/5.6/controllers#resource-controllers
 */
Route::namespace('Admin')->prefix('admin')->group(function () {

	//Define the routes for articles
	Route::resource('/articles', 'ArticleController');
	//Define the routes for authors
	Route::resource('/authors', 'AuthorController');
	//Define the routes for categories
	Route::resource('/categories', 'CategoryController');

	//Define a get route for the admin index page and assign the name admin to the route.
	//Naming the route will help us to easily reference the route in the app
	Route::get('/', 'AdminIndexController@index')->name('admin');
});