<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Here we will define the routes for our API endpoints
 * All routes in this file are automatically prefixed with /api/*
 *
 * Here we also ignore authentication, in real life situation we
 * might want to use tokens to make sure that whoever is trying
 * to access our API has the right to do it
 */


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::namespace('API')->group(function () {

	//Define a route to get all articles in the system
	Route::get('/articles', 'ArticleController@index');
	//Define a route to get a single article for the system given its id
	//An example end point will be domain/articles/1
	Route::get('/articles/{id}', 'ArticleController@show');

	//Define also a route in case user types only /api, to redirect to /articles
	Route::get('/', 'ArticleController@index');
});
